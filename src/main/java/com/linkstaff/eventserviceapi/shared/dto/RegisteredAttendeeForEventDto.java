package com.linkstaff.eventserviceapi.shared.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegisteredAttendeeForEventDto {

	private Long id;
	private Long userId;
	private Long eventId;
	private EventDto event;
}
