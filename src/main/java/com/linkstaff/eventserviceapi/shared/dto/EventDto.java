package com.linkstaff.eventserviceapi.shared.dto;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EventDto {

	private Long id;
	private String name;
	private String description;
	private Date exhibitionDate;

}
