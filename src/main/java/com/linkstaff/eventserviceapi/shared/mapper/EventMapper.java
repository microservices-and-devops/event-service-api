package com.linkstaff.eventserviceapi.shared.mapper;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;

import com.linkstaff.eventserviceapi.io.entity.EventEntity;
import com.linkstaff.eventserviceapi.shared.dto.EventDto;
import com.linkstaff.eventserviceapi.ws.ui.model.request.EventRequestModel;
import com.linkstaff.eventserviceapi.ws.ui.model.response.EventResponseModel;

@Component
public class EventMapper {
	
	private ModelMapper modelMapper;
	TypeMap<EventDto, EventEntity> eventTypeMap;

	public EventMapper() {
		this.modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		this.eventTypeMap = modelMapper.createTypeMap(EventDto.class, EventEntity.class);
	}

	public EventDto mapFromEventRequestModelToEventDto(EventRequestModel eventRequestModel) {
		return modelMapper.map(eventRequestModel, EventDto.class);
	}
	
	public EventDto mapFromEventResponseModelToEventDto(EventResponseModel eventResponseModel) {
		return modelMapper.map(eventResponseModel, EventDto.class);
	}

	public EventDto mapFromEventEntityToEventDto(EventEntity eventEntity) {
		return modelMapper.map(eventEntity, EventDto.class);
	}

	public EventEntity mapFromEventDtoToEventEntity(EventDto eventDto, EventEntity eventEntity) {
		
		eventTypeMap.addMappings(mapper -> {
			mapper.skip(EventEntity::setId);
		});
		eventTypeMap.map(eventDto, eventEntity);
		return eventEntity;

	}
	public EventEntity mapAllFromEventDtoToEventEntity(EventDto eventDto) {
		return modelMapper.map(eventDto, EventEntity.class);
	}

	public EventResponseModel mapFromEventDtoToEventResponseModel(EventDto eventDto) {
		return modelMapper.map(eventDto, EventResponseModel.class);
	}

}
