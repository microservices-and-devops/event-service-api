package com.linkstaff.eventserviceapi.shared.mapper;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;

import com.linkstaff.eventserviceapi.io.entity.RegisteredAttendeeForEventEntity;
import com.linkstaff.eventserviceapi.shared.dto.RegisteredAttendeeForEventDto;
import com.linkstaff.eventserviceapi.ws.ui.model.request.RegisteredAttendeeForEventRequestModel;
import com.linkstaff.eventserviceapi.ws.ui.model.response.RegisteredAttendeeForEventResponseModel;

@Component
public class RegisteredAttendeeForEventMapper {

	private ModelMapper modelMapper;
	TypeMap<RegisteredAttendeeForEventDto, RegisteredAttendeeForEventEntity> registeredAttendeeForEventTypeMap;

	public RegisteredAttendeeForEventMapper() {
		this.modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		this.registeredAttendeeForEventTypeMap = modelMapper.createTypeMap(RegisteredAttendeeForEventDto.class, RegisteredAttendeeForEventEntity.class);
	}

	public RegisteredAttendeeForEventDto mapFromRegisteredEventRequestModelToRegisteredEventDto(RegisteredAttendeeForEventRequestModel registeredAttendeeForEventRequestModel) {
		return modelMapper.map(registeredAttendeeForEventRequestModel, RegisteredAttendeeForEventDto.class);
	}
	
	public RegisteredAttendeeForEventDto mapFromRegisteredEventResponseModelToRegisteredEventDto(RegisteredAttendeeForEventResponseModel registeredAttendeeForEventResponseModel) {
		return modelMapper.map(registeredAttendeeForEventResponseModel, RegisteredAttendeeForEventDto.class);
	}

	public RegisteredAttendeeForEventDto mapFromRegisteredEventEntityToRegisteredEventDto(RegisteredAttendeeForEventEntity registeredAttendeeForEventEntity) {
		return modelMapper.map(registeredAttendeeForEventEntity, RegisteredAttendeeForEventDto.class);
	}

	public RegisteredAttendeeForEventEntity mapFromRegisteredEventDtoToRegisteredEventEntity(RegisteredAttendeeForEventDto registeredAttendeeForEventDto, RegisteredAttendeeForEventEntity registeredAttendeeForEventEntity) {
		
		registeredAttendeeForEventTypeMap.addMappings(mapper -> {
			mapper.skip(RegisteredAttendeeForEventEntity::setId);
		});
		registeredAttendeeForEventTypeMap.map(registeredAttendeeForEventDto, registeredAttendeeForEventEntity);
		return registeredAttendeeForEventEntity;

	}
	public RegisteredAttendeeForEventEntity mapAllFromRegisteredEventDtoToRegisteredEventEntity(RegisteredAttendeeForEventDto registeredAttendeeForEventDto) {
		return modelMapper.map(registeredAttendeeForEventDto, RegisteredAttendeeForEventEntity.class);
	}

	public RegisteredAttendeeForEventResponseModel mapFromRegisteredEventDtoToRegisteredEventResponseModel(RegisteredAttendeeForEventDto registeredAttendeeForEventDto) {
		return modelMapper.map(registeredAttendeeForEventDto, RegisteredAttendeeForEventResponseModel.class);
	}
	
}
