package com.linkstaff.eventserviceapi.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.linkstaff.eventserviceapi.io.entity.EventEntity;
import com.linkstaff.eventserviceapi.io.entity.RegisteredAttendeeForEventEntity;

public interface RegisteredAttendeeForEventRepository extends PagingAndSortingRepository<RegisteredAttendeeForEventEntity, Long> {

	List<RegisteredAttendeeForEventEntity> findAll();
	RegisteredAttendeeForEventEntity findById(long id);
	List<RegisteredAttendeeForEventEntity> findByUserId(long userId);
	RegisteredAttendeeForEventEntity findByUserIdAndEvent(long userId, EventEntity event);
}
