package com.linkstaff.eventserviceapi.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.linkstaff.eventserviceapi.io.entity.EventEntity;


@Repository
public interface EventRepository extends PagingAndSortingRepository<EventEntity, Long>{

	List<EventEntity> findAll();
	EventEntity findById(long id);
}
