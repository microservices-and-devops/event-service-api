package com.linkstaff.eventserviceapi.io.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;

/**
 * This is the event entity which will be persists in the database
 * @author Md Jahangir Hossain Bhuyain
 * @since June 08, 2020
 */
@Entity(name = "event")
@Getter
@Setter
public class EventEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String name;
	private String description;
	private Date exhibitionDate;
	@Column(nullable = false)
	private boolean isDeleted = false;
	
	@OneToMany(mappedBy = "event", cascade = CascadeType.ALL)
	private List<RegisteredAttendeeForEventEntity> registeredAttendees = new ArrayList<RegisteredAttendeeForEventEntity>();
	
	
}
