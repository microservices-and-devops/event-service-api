package com.linkstaff.eventserviceapi.io.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;


/**
 * This is the RegisteredAttendeeForEvent entity which will hold registration information about an event
 * @author Md Jahangir Hossain Bhuyain
 * @since June 08, 2020
 */

@Entity(name = "registerdAttendeeOfEvent")
@Getter
@Setter
public class RegisteredAttendeeForEventEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private Long userId;
	@Column(nullable = false)
	private boolean isDeleted = false;
	
	@ManyToOne
	@JoinColumn(name = "eventId")
	private EventEntity event;
}
