package com.linkstaff.eventserviceapi.ws.ui.model.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegisteredAttendeeForEventRequestModel {
	
	private Long userId;
	private Long eventId;

}
