package com.linkstaff.eventserviceapi.ws.ui.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.linkstaff.eventserviceapi.shared.dto.RegisteredAttendeeForEventDto;
import com.linkstaff.eventserviceapi.shared.mapper.RegisteredAttendeeForEventMapper;
import com.linkstaff.eventserviceapi.ws.service.RegisteredAttendeeForEventService;
import com.linkstaff.eventserviceapi.ws.ui.model.request.RegisteredAttendeeForEventRequestModel;
import com.linkstaff.eventserviceapi.ws.ui.model.response.RegisteredAttendeeForEventResponseModel;

@RestController
@RequestMapping(value = "/")
public class RegisteredAttendeeForEventController {
	
	@Autowired
	RegisteredAttendeeForEventService registeredAttendeeForEventService;
	
	@Autowired
	RegisteredAttendeeForEventMapper registeredAttendeeForEventMapper;
	
	@GetMapping(value = "events/{eventId}/registeredAttendee")
	public List<Long> getRegisteredAttendeeByEventId(
			@RequestParam(value = "page", defaultValue = "1") int page,
			@RequestParam(value = "limit", defaultValue = "25") int limit,
			@RequestParam(value = "sortby", defaultValue = "") String sortBy,
			@RequestParam(value = "searchby", defaultValue = "") String searchBy, 
			@PathVariable Long eventId) {
		
		return registeredAttendeeForEventService.getAllRegisteredAttendeeByEventId(page, limit, eventId);
		
	}
	
	@GetMapping(value = "profiles/{userId}/registeredEvents")
	public List<RegisteredAttendeeForEventResponseModel> getRegisteredEventsByUserId(
			@RequestParam(value = "page", defaultValue = "1") int page,
			@RequestParam(value = "limit", defaultValue = "25") int limit,
			@RequestParam(value = "sortby", defaultValue = "") String sortBy,
			@RequestParam(value = "searchby", defaultValue = "") String searchBy, 
			@PathVariable Long userId) {
		
		List<RegisteredAttendeeForEventDto> registeredAttendeeForEventDtoList = registeredAttendeeForEventService.getAllRegisteredEventInfoByAttendeeId(page, limit, userId);
		List<RegisteredAttendeeForEventResponseModel> registeredAttendeeForEventResponseList = new ArrayList<>();
		for(RegisteredAttendeeForEventDto registeredAttendeeForEventDto: registeredAttendeeForEventDtoList) {
			registeredAttendeeForEventResponseList.add(registeredAttendeeForEventMapper.mapFromRegisteredEventDtoToRegisteredEventResponseModel(registeredAttendeeForEventDto));
		}
		return registeredAttendeeForEventResponseList;
		
	}
	
	@GetMapping(value = "profiles/{userId}/registeredEvents/{eventId}")
	public RegisteredAttendeeForEventResponseModel getGetUserRegisteredEventByEventId(
			@PathVariable Long userId,
			@PathVariable Long eventId) {
		
		RegisteredAttendeeForEventDto registeredAttendeeForEventDto =  registeredAttendeeForEventService.getRegisteredEventInfoByAttendeeIdAndEventId(userId, eventId);
		
		return registeredAttendeeForEventMapper.mapFromRegisteredEventDtoToRegisteredEventResponseModel(registeredAttendeeForEventDto);
	}
	
	@PostMapping(value = "profiles/{userId}/registeredEvents")
	public RegisteredAttendeeForEventResponseModel registerUserToEvent(
			@RequestBody RegisteredAttendeeForEventRequestModel registeredAttendeeForEventRequestModel,
			@PathVariable Long userId) {
		
		RegisteredAttendeeForEventDto registeredAttendeeForEventDto = registeredAttendeeForEventMapper.mapFromRegisteredEventRequestModelToRegisteredEventDto(registeredAttendeeForEventRequestModel);
		registeredAttendeeForEventDto = registeredAttendeeForEventService.createRegisteredAttendeeForEvent(registeredAttendeeForEventDto);
		return registeredAttendeeForEventMapper.mapFromRegisteredEventDtoToRegisteredEventResponseModel(registeredAttendeeForEventDto);
	}

}
