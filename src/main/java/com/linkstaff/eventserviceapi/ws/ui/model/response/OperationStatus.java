package com.linkstaff.eventserviceapi.ws.ui.model.response;

public enum OperationStatus {
	ERROR, SUCCESS
}
