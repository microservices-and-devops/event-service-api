package com.linkstaff.eventserviceapi.ws.ui.model.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegisteredAttendeeForEventResponseModel {
	
	private Long id;
	private Long userId;
	private EventResponseModel event;
}
