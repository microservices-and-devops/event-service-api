package com.linkstaff.eventserviceapi.ws.ui.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.linkstaff.eventserviceapi.shared.dto.EventDto;
import com.linkstaff.eventserviceapi.shared.mapper.EventMapper;
import com.linkstaff.eventserviceapi.ws.service.EventService;
import com.linkstaff.eventserviceapi.ws.ui.model.request.EventRequestModel;
import com.linkstaff.eventserviceapi.ws.ui.model.response.EventResponseModel;
import com.linkstaff.eventserviceapi.ws.ui.model.response.OperationName;
import com.linkstaff.eventserviceapi.ws.ui.model.response.OperationStatus;
import com.linkstaff.eventserviceapi.ws.ui.model.response.OperationStatusModel;



@RestController
@RequestMapping(value = "/")
public class EventController {
	
	@Autowired
	EventService eventService;

	@Autowired
	EventMapper eventMapper;

	@GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<EventResponseModel> getAllEvent(
			@RequestParam(value = "page", defaultValue = "1") int page,
			@RequestParam(value = "limit", defaultValue = "25") int limit) {
		List<EventResponseModel> eventResponseModelList = new ArrayList<>();

		List<EventDto> eventDtoList = eventService.getAllEvents( page, limit);
		
		for (EventDto eventDto : eventDtoList) {
			eventResponseModelList.add(eventMapper.mapFromEventDtoToEventResponseModel(eventDto));
		}

		return eventResponseModelList;
	}

	@GetMapping(path = "/{Id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public EventResponseModel getEventById(@PathVariable long Id) {
		EventDto eventDto = eventService.getEventById(Id);
		return eventMapper.mapFromEventDtoToEventResponseModel(eventDto);
	}

	@PostMapping(consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public EventResponseModel createEvent(
			@RequestBody EventRequestModel eventRequestModel) {
		
		EventDto eventDto = eventMapper.mapFromEventRequestModelToEventDto(eventRequestModel);
		eventDto = eventService.createEvent(eventDto);
		return eventMapper.mapFromEventDtoToEventResponseModel(eventDto);
	}

	@PutMapping(path = "/{id}", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public EventResponseModel updateEventById( @PathVariable long id,
			@RequestBody EventRequestModel eventRequestModel) {
		
		EventDto eventDto = eventMapper.mapFromEventRequestModelToEventDto(eventRequestModel);

		eventDto = eventService.updateEvent(eventDto,id);
		return eventMapper.mapFromEventDtoToEventResponseModel(eventDto);

	}

	@DeleteMapping(path = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public OperationStatusModel deleteEventById( @PathVariable long id) {
		OperationStatusModel operationStatusModel = new OperationStatusModel();
		operationStatusModel.setOperationName(OperationName.DELETE.name());
		
			if (eventService.deleteEventById(id)) {
				operationStatusModel.setOperationResult(OperationStatus.SUCCESS.name());
			} else {
				operationStatusModel.setOperationResult(OperationStatus.ERROR.name());
			}


		return operationStatusModel;
	}

	@DeleteMapping(path = "hardDelete/{id}", produces = { MediaType.APPLICATION_JSON_VALUE })
	public OperationStatusModel hardDeleteEventById( @PathVariable long id) {
		OperationStatusModel operationStatusModel = new OperationStatusModel();
		operationStatusModel.setOperationName(OperationName.DELETE.name());
		eventService.hardDeleteEventById(id);
		operationStatusModel.setOperationResult(OperationStatus.SUCCESS.name());
		return operationStatusModel;
	}

}
