package com.linkstaff.eventserviceapi.ws.ui.model.request;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EventRequestModel {
	
	private String name;
	private String description;
	private Date exhibitionDate;
}
