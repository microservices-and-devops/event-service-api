package com.linkstaff.eventserviceapi.ws.ui.model.response;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EventResponseModel {
	
	private Long id;
	private String name;
	private String description;
	private Date exhibitionDate;

}
