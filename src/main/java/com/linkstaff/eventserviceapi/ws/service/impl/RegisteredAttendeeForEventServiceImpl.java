package com.linkstaff.eventserviceapi.ws.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.linkstaff.eventserviceapi.io.entity.EventEntity;
import com.linkstaff.eventserviceapi.io.entity.RegisteredAttendeeForEventEntity;
import com.linkstaff.eventserviceapi.repository.EventRepository;
import com.linkstaff.eventserviceapi.repository.RegisteredAttendeeForEventRepository;
import com.linkstaff.eventserviceapi.shared.dto.RegisteredAttendeeForEventDto;
import com.linkstaff.eventserviceapi.shared.mapper.RegisteredAttendeeForEventMapper;
import com.linkstaff.eventserviceapi.ws.service.RegisteredAttendeeForEventService;

@Service
public class RegisteredAttendeeForEventServiceImpl implements RegisteredAttendeeForEventService{

	@Autowired
	RegisteredAttendeeForEventRepository registeredAttendeeForEventRepository;
	
	@Autowired
	EventRepository eventRepository;
	
	@Autowired
	RegisteredAttendeeForEventMapper registeredAttendeeForEventMapper;
	@Override
	public List<RegisteredAttendeeForEventDto> getAllRegisteredEventInfoByAttendeeId(int page, int limit,
			long attendeeId) {
		List<RegisteredAttendeeForEventDto> registeredEventDtos = new ArrayList<RegisteredAttendeeForEventDto>();
		List<RegisteredAttendeeForEventEntity> registeredAttendeeForEventEntities = registeredAttendeeForEventRepository.findByUserId(attendeeId);
		
		for(RegisteredAttendeeForEventEntity registeredAttendeeForEventEntity: registeredAttendeeForEventEntities) {
			registeredEventDtos.add(registeredAttendeeForEventMapper.mapFromRegisteredEventEntityToRegisteredEventDto(registeredAttendeeForEventEntity));
		}
		
		return registeredEventDtos;
	}

	@Override
	public List<Long> getAllRegisteredAttendeeByEventId(int page, int limit, long eventId) {
	
		List<Long> registeredEventDtos = new ArrayList<Long>();
		EventEntity eventEntity= eventRepository.findById(eventId);
		
		for(RegisteredAttendeeForEventEntity registeredAttendeeForEventEntity: eventEntity.getRegisteredAttendees()) {
			registeredEventDtos.add(registeredAttendeeForEventEntity.getUserId());
		}
		
		return registeredEventDtos;
	}

	@Override
	public RegisteredAttendeeForEventDto getRegisteredEventInfoByAttendeeIdAndEventId(long attendeeId, long eventId) {
		
		EventEntity eventEntity= eventRepository.findById(eventId);
		
		RegisteredAttendeeForEventEntity registeredAttendeeForEventEntity = registeredAttendeeForEventRepository.findByUserIdAndEvent(attendeeId, eventEntity);
		
		return registeredAttendeeForEventMapper.mapFromRegisteredEventEntityToRegisteredEventDto(registeredAttendeeForEventEntity);
	}

	@Override
	public RegisteredAttendeeForEventDto createRegisteredAttendeeForEvent(
			RegisteredAttendeeForEventDto registeredAttendeeForEventDto) {
		EventEntity eventEntity= eventRepository.findById(registeredAttendeeForEventDto.getEventId()).get();
		if(eventEntity == null)
			throw new RuntimeException("Event Record: not found");
		RegisteredAttendeeForEventEntity registeredAttendeeForEventEntity = new RegisteredAttendeeForEventEntity();
		
		if(eventEntity.getRegisteredAttendees()==null) {
			List<RegisteredAttendeeForEventEntity> registeredAttendeeForEventEntities = new ArrayList<>();
			registeredAttendeeForEventEntities.add(registeredAttendeeForEventEntity);
			eventEntity.setRegisteredAttendees(registeredAttendeeForEventEntities);
		}else {}
		
		registeredAttendeeForEventEntity.setEvent(eventEntity);
		registeredAttendeeForEventEntity.setUserId(registeredAttendeeForEventDto.getUserId());
		
		registeredAttendeeForEventEntity = registeredAttendeeForEventRepository.save(registeredAttendeeForEventEntity);
		return registeredAttendeeForEventMapper.mapFromRegisteredEventEntityToRegisteredEventDto(registeredAttendeeForEventEntity);
		
	}

	@Override
	public RegisteredAttendeeForEventDto updateRegisteredAttendeeForEvent(
			RegisteredAttendeeForEventDto registeredAttendeeForEventDto, long id) {
		
		RegisteredAttendeeForEventEntity registeredAttendeeForEventEntity = registeredAttendeeForEventRepository.findById(id);
		if (registeredAttendeeForEventEntity == null)
			throw new RuntimeException("RegisteredAttendeeForEventEntity Record: not found");
		
		registeredAttendeeForEventEntity = registeredAttendeeForEventMapper.mapFromRegisteredEventDtoToRegisteredEventEntity(registeredAttendeeForEventDto, registeredAttendeeForEventEntity);
		registeredAttendeeForEventEntity = registeredAttendeeForEventRepository.save(registeredAttendeeForEventEntity);

		return registeredAttendeeForEventMapper.mapFromRegisteredEventEntityToRegisteredEventDto(registeredAttendeeForEventEntity);
	}

	@Override
	public boolean deleteRegisteredAttendeeForEventById(long id) {
		RegisteredAttendeeForEventEntity registeredAttendeeForEventEntity = registeredAttendeeForEventRepository.findById(id);
		if (registeredAttendeeForEventEntity == null)
			throw new RuntimeException("RegisteredAttendeeForEventEntity Record: not found");
		
		registeredAttendeeForEventEntity.setDeleted(true);

		if (registeredAttendeeForEventRepository.save(registeredAttendeeForEventEntity).isDeleted()) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean hardDeleteRegisteredAttendeeForEventById(long id) {
		RegisteredAttendeeForEventEntity registeredAttendeeForEventEntity = registeredAttendeeForEventRepository.findById(id);
		if (registeredAttendeeForEventEntity == null)
			throw new RuntimeException("RegisteredAttendeeForEventEntity Record: not found");
		
		registeredAttendeeForEventRepository.delete(registeredAttendeeForEventEntity);
		return true;
	}

}
