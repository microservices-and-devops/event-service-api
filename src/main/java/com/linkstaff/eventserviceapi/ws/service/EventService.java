package com.linkstaff.eventserviceapi.ws.service;

import java.util.List;

import com.linkstaff.eventserviceapi.shared.dto.EventDto;

public interface EventService {
	
	public List<EventDto> getAllEvents(int page, int limit);
	public EventDto getEventById(long id);
	public EventDto createEvent(EventDto eventDto);
	public EventDto updateEvent(EventDto eventDto, long id);
	public boolean deleteEventById(long id);
	public boolean hardDeleteEventById(long id);

}
