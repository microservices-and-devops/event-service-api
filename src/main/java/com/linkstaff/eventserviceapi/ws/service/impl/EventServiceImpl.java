package com.linkstaff.eventserviceapi.ws.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.linkstaff.eventserviceapi.io.entity.EventEntity;
import com.linkstaff.eventserviceapi.repository.EventRepository;
import com.linkstaff.eventserviceapi.shared.dto.EventDto;
import com.linkstaff.eventserviceapi.shared.mapper.EventMapper;
import com.linkstaff.eventserviceapi.ws.service.EventService;


@Service
public class EventServiceImpl implements EventService {
	
	@Autowired
	EventRepository eventRepository;
	
	@Autowired
	EventMapper eventMapper;

	@Override
	public List<EventDto> getAllEvents(int page, int limit) {
		List<EventDto> eventDtoList = new ArrayList<>();

		List<EventEntity> events = eventRepository.findAll();

		for (EventEntity eventEntity : events) {
			eventDtoList.add(eventMapper.mapFromEventEntityToEventDto(eventEntity));
		}
		return eventDtoList;
	}

	@Override
	public EventDto getEventById(long id) {
		EventEntity eventEntity = eventRepository.findById(id);
		if (eventEntity == null)
			throw new RuntimeException("Event Record: not found");
		return eventMapper.mapFromEventEntityToEventDto(eventEntity);
	}

	@Override
	public EventDto createEvent(EventDto eventDto) {
		
		EventEntity eventEntity = eventMapper.mapAllFromEventDtoToEventEntity(eventDto);
		eventEntity = eventRepository.save(eventEntity);
		return eventMapper.mapFromEventEntityToEventDto(eventEntity);
	}

	@Override
	public EventDto updateEvent(EventDto eventDto, long id) {
		
		EventEntity eventEntity = eventRepository.findById(id);
		if (eventEntity == null)
			throw new RuntimeException("Event Record: not found");
	
		eventEntity = eventMapper.mapFromEventDtoToEventEntity(eventDto, eventEntity);
		eventEntity = eventRepository.save(eventEntity);
		return eventMapper.mapFromEventEntityToEventDto(eventEntity);
	}

	@Override
	public boolean deleteEventById(long id) {
		
		EventEntity eventEntity = eventRepository.findById(id);
		if (eventEntity == null)
			throw new RuntimeException("Event Record: not found");
		
		eventEntity.setDeleted(true);
		
		if (eventRepository.save(eventEntity).isDeleted()) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean hardDeleteEventById(long id) {
		
		EventEntity eventEntity = eventRepository.findById(id);
		if (eventEntity == null)
			throw new RuntimeException("Event Record: not found");

		eventRepository.delete(eventEntity);
		return true;
	}

}
