package com.linkstaff.eventserviceapi.ws.service;

import java.util.List;

import com.linkstaff.eventserviceapi.shared.dto.RegisteredAttendeeForEventDto;


public interface RegisteredAttendeeForEventService {
	
	public List<RegisteredAttendeeForEventDto> getAllRegisteredEventInfoByAttendeeId(int page, int limit, long attendeeId);
	public List<Long> getAllRegisteredAttendeeByEventId(int page, int limit, long eventId);
	public RegisteredAttendeeForEventDto getRegisteredEventInfoByAttendeeIdAndEventId(long attendeeId, long eventId);
	public RegisteredAttendeeForEventDto createRegisteredAttendeeForEvent(RegisteredAttendeeForEventDto registeredAttendeeForEventDto);
	public RegisteredAttendeeForEventDto updateRegisteredAttendeeForEvent(RegisteredAttendeeForEventDto registeredAttendeeForEventDto, long id);
	public boolean deleteRegisteredAttendeeForEventById(long id);
	public boolean hardDeleteRegisteredAttendeeForEventById(long id);

}
