package com.linkstaff.eventserviceapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EventServiceApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(EventServiceApiApplication.class, args);
	}

}
