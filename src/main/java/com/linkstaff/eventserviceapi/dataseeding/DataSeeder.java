package com.linkstaff.eventserviceapi.dataseeding;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.github.javafaker.Faker;
import com.linkstaff.eventserviceapi.io.entity.EventEntity;
import com.linkstaff.eventserviceapi.io.entity.RegisteredAttendeeForEventEntity;
import com.linkstaff.eventserviceapi.repository.EventRepository;

@Component
public class DataSeeder implements CommandLineRunner {
	
	@Autowired
	EventRepository eventRepository;

	@Override
	public void run(String... args) throws Exception {
		
		
		List<RegisteredAttendeeForEventEntity> registeredAttendees;
		
		Faker faker = new Faker(new Random(50));
		int MAX_ITEM = 200;
		int MAX_NESTED_ITEM = 50;
		
		for (int i=0; i < MAX_ITEM; i++) {
			EventEntity eventEntity = new EventEntity();
			
			eventEntity.setDescription(faker.gameOfThrones().quote());
			eventEntity.setExhibitionDate(DateUtils.addDays(new Date(), i+20));
			eventEntity.setName(faker.gameOfThrones().character());
			
			
			registeredAttendees = new ArrayList<>();
			
			for(int j=0; j < MAX_NESTED_ITEM; j++) {
				
				//Setting value for RegisteredAttendeeForEventEntity
				RegisteredAttendeeForEventEntity registeredAttendeeForEventEntity = new RegisteredAttendeeForEventEntity();
				registeredAttendeeForEventEntity.setUserId(Long.valueOf(i+j));
				registeredAttendeeForEventEntity.setEvent(eventEntity);
				registeredAttendees.add(registeredAttendeeForEventEntity);
				
			}
			
			eventEntity.setRegisteredAttendees(registeredAttendees);
	    	eventRepository.save(eventEntity);
			
		}
		
		

	}

}
